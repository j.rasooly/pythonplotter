# Hinweis: 
       Der Ordner plotProject_venv ist bereits für die Verwendung unter Windows angelegt worden.
	   Bei Problemen oder einem abweichenden Betriebssystem sollte der Ordner gelöscht und mit
	   den aufgeführten Eingaben neu erstellt werden.

# Programm-Start unter Windows

1. Herunterladen des ZIP-Ordners
2. Entpacken des ZIP-Ordners (z.B. in den Download-Ordner)
3. Öffnen der Eingabeaufforderung
4. Verzeichnis ändern:
	cd C:<Verzeichnispfad>/pyt_WS_22_23_Otto_Pascal_und_Rasooly_Jalil
5. Virtuelle Umgebung erzeugen (falls der Ordner noch nicht existiert):
	py -m venv plotProject_venv
6. Virtuelle Umgebung aktivieren:
	plotProject_venv\Scripts\activate
7. Erforderliche Pakete installieren (falls Pakete noch nicht installiert worden sind):
	py -m pip install -r requirements.txt
8. Programm ausführen:
	py src_plotProject/test.py
9. Deaktivierung der virtuellen Umgebung:
	plotProject\Scripts\deactivate



# Programm-Start unter Mac-OS

1. Herunterladen des ZIP-Ordners
2. Entpacken des ZIP-Ordners (z.B. in den Download-Ordner)
3. Öffnen der Terminals:
4. Verzeichnis ändern:
	cd <Verzeichnispfad>/pyt_WS_22_23_Otto_Pascal_und_Rasooly_Jalil
5. Virtuelle Umgebung erzeugen (falls der Ordner noch nicht existiert):
	python -m venv plotProject_venv
6. Virtuelle Umgebung aktivieren:
	source plotProject_venv\bin\activate
7. Erforderliche Pakete installieren (falls Pakete noch nicht installiert worden sind):
	python -m pip install -r requirements.txt
	python -m pip install matplotlib (bei Fehlermeldung zusätzlich ausführen)
8. Programm ausführen:
	python src_plotProject/test.py
9. Deaktivierung der virtuellen Umgebung:
	deactivate

